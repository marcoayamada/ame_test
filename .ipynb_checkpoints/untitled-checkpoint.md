## LATAM

- 24 países diferentes
    - Distribuídos em 4 regiões: Norte, Caribe, Central e Sul
    - Desde 2011 tem aumento nos lucro e quantidade de pedidos
    - É o quarto maior Market a dar lucro e o segundo maior a fazer pedidos
    - Profits
        - México é o maior mercado
        - No Caribe, Cuba é o mercado mais significativo
        - Valor de prejuízo causado pelos países é próximo a todo o lucro do México (94k vs 100k do méxico)
        - Países que dão prejuízo tem um histórico longo de perdas.
        - Brasil fica em cima do muro. Tem momentos de lucro e outro de prejuízo, mas utimamente vem dando resultados positivos
        - São inversamente proporcionais categorias de produtos e soma do lucro.
        - Nenhum desconto é dado para 58% da base latam
        - Quantidade de itens por pedido tem uma média de 3 por pedido
        - Das 5 primeiras sub-categorias, 3 são de tecnologia represetando 44% do lucro
            - Mesa gera apenas prejuízo
        - Das 6 primeiras
            - México: Technologia. Quase a metade é com Copiers
            - El Salavador: móveis. Quase 20k, 8k cadeira
            - CUba: móveis, De quase 20k, 8k cadeira
            - Brasil: de 11k, quase 9k é com cadeira
            - Nicargua: de 12k, 8k é bookcase
            - gautemala: quase metade é bookcase


































_________________________
- Category mesa está dando prejuízo
    - Viés para quem tem desconto
    - Por que dar desconto nesse item?
    
    
    
- Big numbers
    - 3788 produtos diferentes
        - distribuídos em 3 grandes categorias: Furnitures, Office suppliers e Technology
        - dentro desses 3 grandes grupos tem-se 17 sub categorias diferentes
    - Vendas para 147 países diferentes
    - Lucro de vem aumentando ano a ano
        - de 20% a 30%
    - Pareto
        - ~30% vem de Copies e celphones
        - Office supliers não tem um valor agregado alto, mas somados vendem bem
        - Tables estão dando prejuízo
        - Cadeiras e bookcases vendem bem também

- Como aumentar o lucro?
    - Identificar produtos que dão prejuízo
    - Mercados que dão prejuízo
    - Diminuir taxas de envio
   